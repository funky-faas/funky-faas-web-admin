package garden.bots

import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import garden.bots.allowed.*

private fun RoutingContext.json(jsonObject: JsonObject) {
  this.response().putHeader("content-type", "application/json;charset=UTF-8").end(jsonObject.encodePrettily())
}

class Admin : RoutePlugin {

  override fun about(): String {
    return """
      ------------------------------------------------------
        👋 This is the Administration plug-in of Funky-FaaS
        ❤️ The diskless FaaS framework (with Kotlin)
        Version: ${this.version()}
      ------------------------------------------------------
    """.trimIndent()
  }

  override fun version(): String {
    return "0.0.0"
  }

  override fun define(router: Router) {

    router.get("/admin").handler { context ->
      context.json(JsonObject().put("message", "admin!!!"))
    }
  }
}